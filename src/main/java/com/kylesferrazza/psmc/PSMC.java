package com.kylesferrazza.psmc;

import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PSMC extends JavaPlugin implements Listener {

  private ArrayList<Villager> villagers = new ArrayList<Villager>();

  @Override
  public void onEnable() {
    getServer().getPluginManager().registerEvents(this, this);
    WorldCreator wc = new WorldCreator("psmc");
    wc.environment(World.Environment.NORMAL);
    wc.generateStructures(false);
    wc.type(WorldType.FLAT);
    World w = Bukkit.getServer().createWorld(wc);
    double newY = 100;
    for (String process : getProcesses()) {
      Location l = w.getSpawnLocation();
      Entity ent = w.spawnEntity(l, EntityType.VILLAGER);
      Villager v = (Villager) ent;
      v.setCustomName(process);
      v.setCustomNameVisible(true);
      villagers.add(v);
      System.out.println("[psmc] "
            + "\"" + v.getCustomName() + "\" VILLAGER AT "
            + "X=" + v.getLocation().getBlockX() + ", "
            + "Y=" + v.getLocation().getBlockY() + ", "
            + "Z=" + v.getLocation().getBlockZ() + ", "
            + "IN WORLD " + v.getWorld().getName());
    }
    super.onEnable();
  }

  @Override
  public void onDisable() {
    for (Villager v : villagers) {
      System.out.println("[psmc] killing \"" + v.getCustomName() + "\" villager");
      v.remove();
    }
    super.onDisable();
  }

  private ArrayList<String> getProcesses() {
    ArrayList<String> ps = new ArrayList<String>();
    try {
      String line;
      Process p = Runtime.getRuntime().exec("ps -x -o command");
      BufferedReader input =
            new BufferedReader(new InputStreamReader(p.getInputStream()));
      while ((line = input.readLine()) != null) {
        String a = line.replaceAll(" .*", "");
        ps.add(a);
      }
    } catch (IOException io) {
      io.printStackTrace();
    }
    return ps;
  }

  @EventHandler
  public void onDeath(EntityDeathEvent e) {
    Entity ent = e.getEntity();
    if (ent instanceof Villager && villagers.contains(ent)) {
      Villager villager = (Villager) ent;
      try {
        System.out.println("killing " + villager.getCustomName());
        Runtime.getRuntime().exec("killall " + villager.getCustomName());
      } catch (IOException e1) {
        System.out.println("Couldn't kill " + villager.getCustomName());
      }
    }
  }
}
