# PSMC
A process manager for minecraft. All running processes are spawned in the "psmc" world as Villagers.

Upon the death of one of these villagers, the process with which it shares its name will be killed.

![Screenshot](scrot.png)
